// DTO - Data Transfer Object.
// Data that are sent in. Used in @Body.

export class UserDTO {
    readonly firstName: string;
    readonly lastName: string;
    readonly email: string;
    readonly username: string;
    readonly password: string;
    readonly createdOn: Date;
}
