import { Controller, Post, Res, Body, HttpStatus, Get, Param } from '@nestjs/common';
import { UsersService } from './users.service';
import { UserDTO } from './dto/user.dto';

@Controller('users')
export class UsersController {

    constructor(private userService: UsersService) { }

    // add a new user
    @Post('login')
    async addUser(@Res() res, @Body() userDto: UserDTO) {
        const msg = await this.userService.createUser(userDto);
        return res.status(HttpStatus.OK).json(msg);
    }

    // retrieve users list
    @Get('userlist')
    async getAllUsers(@Res() res) {
        const msg = await this.userService.findAll();
        return res.status(HttpStatus.OK).json(msg);
    }

    // retrive single user
    @Get(':userID')
    async getUser(@Res() res, @Param('userID') userID) {
        const msg = await this.userService.getUser(userID);
        return res.status(HttpStatus.OK).json(msg);
    }
}
