import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './interface/user.interface';
import { UserDTO } from './dto/user.dto';

@Injectable()
export class UsersService {

    constructor(
        @InjectModel('User') private readonly userModel: Model<User>) { }

    // user sign up
    async createUser(userDto: UserDTO): Promise<any> {
        try {
            const newUser = new this.userModel(userDto);
            const result = await newUser.save();
            return { status: 'Success', result };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    // get userlist
    async findAll(): Promise<any[]> {
        return this.userModel.find().exec();
    }

    // get single user
    async getUser(userID): Promise<any> {
        try {
            const result = await this.userModel.findById(userID);
            return { status: 'Success', result };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }
}
