import { Controller, Res, Body, HttpStatus, Post, Get, Param, Put, Delete, Query, Request } from '@nestjs/common';
import { SuppliersService } from './suppliers.service';
import { SuppliersDTO } from './dto/suppliers.dto';
import { request } from 'http';

@Controller('suppliers')
export class SuppliersController {

    constructor(private supplierService: SuppliersService) { }

    // add new supplier
    @Post()
    async addSupplier(@Res() res, @Body() supplierDto: SuppliersDTO) {
        const msg = await this.supplierService.createSupplier(supplierDto);
        return res.status(HttpStatus.OK).json(msg);
    }

    // retrieve supplier list
    @Get()
    async getAllSuppliers(@Res() res) {
        const msg = await this.supplierService.searchAll();
        return res.status(HttpStatus.OK).json(msg);
    }

    // retrive single supplier
    @Get(':supplierID')
    async getSupplier(@Res() res, @Param('supplierID') supplierID) {
        const msg = await this.supplierService.getSupplier(supplierID);
        return res.status(HttpStatus.OK).json(msg);
    }

    // update supplier

    @Put('/update')
    async updateSupplier(
        @Request() req,
        @Res() res,
        @Query('supplierID') supplierID,
        @Body() supplierDto: SuppliersDTO) {

        const msg = await this.supplierService.updateSupplier(
            supplierID,
            supplierDto,
            req.suppliers,
        );
        if (msg.status === 'Failed') {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(msg);
        }
        return res.status(HttpStatus.OK).json(msg);
    }

    // delete supplier
    @Delete('/delete')
    async deleteProductType(@Request() req, @Res() res, @Query('supplierID') supplierID) {
        const msg = await this.supplierService.deleteSupplier(supplierID, req.supplier);
        if (msg.status === 'Failed') {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(msg);
        }

    }

}
