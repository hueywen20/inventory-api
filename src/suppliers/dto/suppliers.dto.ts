// DTO - Data Transfer Object.
// Data that are sent in. Used in @Body.

export class SuppliersDTO {
    readonly fullName: string;
    readonly email: string;
    readonly contact: string;
    readonly address: string;
    readonly isActive: string;
    readonly createdOn: Date;
    readonly createdBy: string;
    modifiedBy: string;
    modifiedOn: Date;
}
