import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Suppliers } from './interface/suppliers.interface';
import { SuppliersDTO } from './dto/suppliers.dto';

@Injectable()
export class SuppliersService {

    constructor(
        @InjectModel('Suppliers') private readonly suppliersModel: Model<Suppliers>,
    ) { }

    // add suppliers
    async createSupplier(supplierDto: SuppliersDTO): Promise<any> {
        try {
            const newSupplier = new this.suppliersModel(supplierDto);
            const result = await newSupplier.save();
            return { status: 'Success', result };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    // get supplier lists
    async searchAll(): Promise<any[]> {
        return this.suppliersModel.find().exec();
    }

    // get single supplier
    async getSupplier(supplierID): Promise<any> {
        try {
            const result = await this.suppliersModel.findById(supplierID);
            return { status: 'Success', result };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    // update supplier
    async updateSupplier(supplierID, modifiedBy, supplierDto: SuppliersDTO): Promise<any> {
        try {
            let result;
            supplierDto.modifiedBy = modifiedBy;
            supplierDto.modifiedOn = new Date(Date.now());
            result = await this.suppliersModel.findByIdAndUpdate(
                supplierID, supplierDto,
                { new: true, runValidators: true },
            );
            return { status: 'Success', result };

        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }

    // delete supplier
    async deleteSupplier(supplierID, modifiedBy): Promise<any> {
        try {
            const deleteSupplier = await this.suppliersModel.findByIdAndUpdate(
                supplierID,
                { isActive: false, modifiedBy, modifiedDate: new Date(Date.now()) },
                { new: true, runValidators: true },
            );
            return { status: 'Success', result: deleteSupplier };
        } catch (err) {
            return { status: 'Failed', message: err.message };
        }
    }
}
