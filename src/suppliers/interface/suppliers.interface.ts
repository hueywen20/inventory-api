import { Document } from 'mongoose';

export interface Suppliers extends Document {
    readonly fullName: string;
    readonly contact: string;
    readonly email: string;
    readonly address: string;
    readonly isActive: string;
    readonly createdOn: Date;
    readonly createdBy: string;
    readonly modifiedBy: string;
    readonly modifiedOn: Date;
}
