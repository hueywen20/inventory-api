import * as mongoose from 'mongoose';

export const SuppliersSchema = new mongoose.Schema({
    fullName: { type: String, required: true },
    contact: { type: String, required: true },
    email: { type: String, required: true },
    address: { type: String, required: true },
    isActive: { type: Boolean },
    createdOn: { type: Date, default: Date.now },
    createdBy: { type: String },
    modifiedBy: { type: String },
    modifiedOn: { type: Date },
});
