import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SuppliersService } from './suppliers.service';
import { SuppliersController } from './suppliers.controller';
import { SuppliersSchema } from './schema/suppliers.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Suppliers', schema: SuppliersSchema }]),

  ],
  providers: [SuppliersService],
  controllers: [SuppliersController],
})
export class SuppliersModule { }
